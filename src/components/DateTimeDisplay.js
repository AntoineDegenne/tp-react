import { useEffect, useState } from "react";

function DateTimeDisplay() {
  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date());
  const [isRunning, setIsRunning] = useState(true);

  useEffect(() => {
    let interval;

    if(isRunning) {
        setDate(new Date());
        setTime(new Date());
        interval = setInterval(() => {
            setTime(new Date());
            setDate(new Date());
          }, 500);
    }

    return function cleanup() {
        clearInterval(interval);
    }

  }, [isRunning]);

  function handleStart() {
    setIsRunning(true);
  }

  function handleStop() {
    setIsRunning(false);
  }

  return (
    <div className="dateTimeDisplay">
      <p>
        Date : <span>{date.toLocaleDateString("fr")}</span>
      </p>
      <p>
        Heure : <span>{time.toLocaleTimeString("fr")}</span>
      </p>
      <div>
        <button onClick={() => handleStart()}>Start</button>
        <button onClick={() => handleStop()}>Stop</button>
      </div>
    </div>
  );
}

export default DateTimeDisplay;
