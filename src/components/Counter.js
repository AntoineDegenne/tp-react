import { useState } from "react";

function CounterLabel({ count }) {
  return <label>Vous avez cliqué {count} fois</label>;
}

function CounterButton({ onAdd }) {
  return <button onClick={onAdd}>Compteur</button>;
}

function Counter() {
  const [count, setCount] = useState(0);

  function handleAdd() {
    setCount(count + 1);
  }

  return (
    <div className="counter">
      <CounterLabel count={count} />
      <CounterButton onAdd={() => handleAdd()} />
    </div>
  );
}

export default Counter;
