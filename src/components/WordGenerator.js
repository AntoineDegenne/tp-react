import { useState, useEffect } from "react";

function WordGenerator() {
  const [firstWord, setFirstWord] = useState("");
  const [secondWord, setSecondWord] = useState("");
  const [isloading, setIsloading] = useState(false);

  useEffect(() => {
    if (!isloading) {
      setIsloading(true);
    } else {
      alert("click1");
    }
    console.log("useEffect 4 ...");
  }, [firstWord]);
  useEffect(() => {
    if (!isloading) {
      setIsloading(true);
    } else {
      alert("click2");
    }
    console.log("useEffect 3 ...");
  }, [secondWord]);
  useEffect(() => {
    if (!isloading) {
        setIsloading(true);
      } else {
        alert("click");
      }
    console.log("useEffect 2 ...");
  }, [firstWord, secondWord]); // tableau de dépendances à des variables d'état vide !
  useEffect(() => {

    console.log("useEffect 1 ...");
  });

  function handleClick1() {
    setFirstWord(
      Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
    );
  }

  function handleClick2() {
    setSecondWord(
      Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
    );
  }

  return (
    <div className="wordGenerator">
      <div>
        {firstWord} - {secondWord}
      </div>
      <button id="button1" onClick={handleClick1}>
        Changer premier mot
      </button>
      <button id="button2" onClick={handleClick2}>
        Changer second mot
      </button>
    </div>
  );
}

export default WordGenerator;
