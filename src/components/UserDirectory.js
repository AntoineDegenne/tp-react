import { useEffect, useState } from "react";

function UserCard({ user }) {
  return (
    <div className="userCard">
      <p>
        <span className="bold">{user.firstName + " " + user.lastName}</span>
      </p>
      <p>
        <span className="bold">Email: </span>
        {user.mail}
      </p>
      <p>
        <span className="bold">Phone: </span>
        {user.phone}
      </p>
      <p>
        <span className="bold">Address: </span>
        {user.address}
      </p>
      <p>
        <span className="bold">City: </span>
        {user.city}
      </p>
      <p>
        <span className="bold">Zip-code: </span>
        {user.zipCode}
      </p>
    </div>
  );
}

function UserDirectory() {
  const [userList, setUserList] = useState([]);
  const cardsList = userList.map((element, i) => (
    <UserCard key={"card" + i} user={element} />
  ));

  useEffect(() => {
    async function fetchUsers() {
      await fetch("http://localhost:8000/users")
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setUserList(data);
        })
        .catch((err) => {
          console.log(err.message);
        });
    }

    fetchUsers();
  }, []);

  return <div className="cardsList">{cardsList}</div>;
}

export default UserDirectory;
