import { useState } from "react";

function ListInput({inputText, handleInputChange, handleClick}) {
    return (
        <div className="listInput">
            <input type="text" value={inputText} onChange={handleInputChange} ></input>
            <button type="button" onClick={handleClick}>Ajouter</button>
        </div>
    )
}

function List({elements}) {

    const listElements = elements.map((element, i) => <li key={'list' + i}>{element}</li>);
    return (
        <ul>
            {listElements}
        </ul>
    )
}

function ListComponent() {

    const [arrayElements, setArrayElements] = useState([])
    const [inputText, setInputText] = useState('');

    function handleClick() {
        const newElement = inputText;
        setArrayElements([...arrayElements, newElement]);
        setInputText('');
    }

  return (
    <div className="list">
      <ListInput inputText={inputText} handleInputChange={e => setInputText(e.target.value)} handleClick={(e) => handleClick(e)}/>
      <List elements={arrayElements}/>
    </div>
  );
}

export default ListComponent;
