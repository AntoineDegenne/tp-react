import logo from './logo.svg';
import Counter from './components/Counter'
import ListComponent from './components/List';
import './App.scss';
import WordGenerator from './components/WordGenerator';
import DateTimeDisplay from './components/DateTimeDisplay';
import UserDirectory from './components/UserDirectory';

function App() {
  return (
    <div className="App">
      <Counter />
      <ListComponent />
      <WordGenerator />
      <DateTimeDisplay />
      <UserDirectory />
    </div>
  );
}

export default App;
